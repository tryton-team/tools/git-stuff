#!/bin/sh

## Copyright (C) 2006-2014 Daniel Baumann <mail@daniel-baumann.ch>
## Copyright (C) 2013-2014 Mathias Behrle <mathiasb@m9s.biz>
##
## This program comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.


set -e

_MODE="$(basename ${0} | sed -e 's|^git-||' -e 's|-release$||')"

if [ ! -e debian/changelog ]
then
	echo "E: no debian/changelog found."
	exit 1
fi

if [ -n "${1}" ]
then
	_VERSION="${1}"
else
	_VERSION="$(dpkg-parsechangelog | awk '/^Version: / { print $2 }' | cut -d: -f 2-)"
fi

_HASH="${2}"

## For our own use we only commit debian/changelog, it should be the only file, that should
## be changed for a release.
# git add .

## We sign release commits
# git commit -a -m "Releasing ${_MODE} version ${_VERSION}."
git commit debian/changelog -s -S -m "Releasing ${_MODE} version ${_VERSION}."

## Example: Use explicit date for changelog:
#git commit debian/changelog --date="Wed Oct 3 23:00:00 2012 +0200" -m "Releasing ${_MODE} version ${_VERSION}."

git-${_MODE}-tag ${_VERSION} ${_HASH}
