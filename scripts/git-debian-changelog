#!/bin/sh

## Copyright (C) 2006-2014 Daniel Baumann <mail@daniel-baumann.ch>
## This program comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.

## Copyright (C) 2011-2014 Mathias Behrle <mathiasb@m9s.biz>
##
## Adding more options for special use cases
## Depends on accordingly customized git-dch, that must be found in PATH

set -e

_MODE="$(basename ${0} | sed -e 's|^git-||' -e 's|-changelog$||')"

# Checking arguments
case "${1}" in
	-h|--help)
		echo "git-${_MODE}-changelog - wrapper over git-dch(1)"
		echo
		echo "Usage: ${0} [ID] [VERSION] [DISTRIBUTION] [ALLOW_LOWER_VERSION] [NO_EDITOR] [URGENCY]"

		exit 0
		;;
esac

# Checking depends
if [ ! -x "$(which git 2>/dev/null)" ]
then
	echo "E: git - No such file."
	exit 1
fi

if [ ! -x "$(which git-dch 2>/dev/null)" ]
then
	echo "E: git-dch - No such file."
	exit 1
fi

if [ ! -f debian/changelog ]
then
	echo "E: debian/changelog - No such file."
	exit 1
fi

# Check for git-dch wrap script
_WRAP_SCRIPT="/usr/share/doc/git-buildpackage/examples/wrap_cl.py"

if [ -e "/usr/share/doc/git-buildpackage/examples/wrap_cl.py" ]
then
       _CUSTOMIZATION="--customization ${_WRAP_SCRIPT}"
fi

COMMIT="${1}"
VERSION="${2}"
DISTRIBUTION="${3}"
ALLOW_LOWER_VERSION="${4}"
NO_EDITOR="${5}"
URGENCY="${6}"
#NO_EDITOR="Never"
## Run with a fixed version
#VERSION="4.0.1-1"
#VERSION="2.6.0-1"
#VERSION="2.8.3-1"
#VERSION="2.2.1-1"
#VERSION="2.0.0+dfsg-1"
#VERSION="1.8.3-0~60squeeze1"
## Run with a fixed distribution
#DISTRIBUTION="squeeze-backports"
#DISTRIBUTION="wheezy-backports"
#DISTRIBUTION="experimental"
#DISTRIBUTION="unstable"

if [ ! -z "${VERSION}" ]
then
	VERSION=" --new-version ${VERSION}"
        echo 'Using fixed version: '${VERSION}
fi

if [ ! -z "${DISTRIBUTION}" ]
then
	#DISTRIBUTION=" --distribution ${DISTRIBUTION} --force-distribution"
	DISTRIBUTION=" --distribution ${DISTRIBUTION}"
        echo 'Using distribution: '${DISTRIBUTION}
fi

if [ ! -z "${URGENCY}" ]
then
	URGENCY=" --urgency ${URGENCY}"
        echo 'Using urgency: '${URGENCY}
fi

if [ ! -z "${ALLOW_LOWER_VERSION}" ]
then
        #ALLOW_LOWER_VERSION=" --allow-lower-version"
        ALLOW_LOWER_VERSION=" --dch-opt=-b --dch-opt=--allow-lower-version"
        echo 'Setting allowed lower version: '${ALLOW_LOWER_VERSION}
fi

if [ ! -z "${NO_EDITOR}" ]
then
        NO_EDITOR=" --spawn-editor=never"
        echo 'Setting --spawn-editor='never': no editor will be spawned, use with care!'
fi

if [ -z "${COMMIT}" ]
then
	# Try to automatically get a commit id from last release commits
	if [ -z "${COMMIT}" ] && [ -n "$(git log | grep -m1 "Releasing ${_MODE} version ")" ]
	then
		COMMIT="$(git log | grep -B4 -m1 "Releasing ${_MODE} version " | awk '/^commit / { print $2 }')"
	fi

	if [ -z "${COMMIT}" ] && [ -n "$(git log | grep -m1 "Adding ${_MODE} version ")" ]
	then
		COMMIT="$(git log | grep -B4 -m1 "Adding ${_MODE} version " | awk '/^commit / { print $2 }')"
	fi
	if [ -z "${COMMIT}" ] && [ -n "$(git log | grep -m1 "Adding upstream version ")" ]
	then
		COMMIT="$(git log | grep -B4 -m1 "Adding upstream version " | awk '/^commit / { print $2 }')"
	fi
	if [ -z "${COMMIT}" ] && [ -n "$(git log | grep -m1 'Releasing version ')" ]
	then
		COMMIT="$(git log | grep -B4 -m1 "Releasing version " | awk '/^commit / { print $2 }')"
	fi
	if [ -n "$(git log | grep -m1 'Adding version ')" ]
	then
		COMMIT="$(git log | grep -B4 -m1 "Adding version " | awk '/^commit / { print $2 }')"
	fi
	if [ -z "${COMMIT}" ] && [ -n "$(git log | grep -m1 "Initial packaging")" ]
	then
		COMMIT="$(git log | grep -B4 -m1 "Initial packaging" | awk '/^commit / { print $2 }')"
	fi
else
	shift
fi
#echo ${COMMIT}
#echo ${@}
#exit 0
if [ -n "${COMMIT}" ]
then
	# org: git-dch --debian-branch $(git branch | awk '/^\* / { print $2 }') --git-author --release --since ${COMMIT} ${_CUSTOMIZATION} ${@}
	#git-dch --debian-branch $(git branch | awk '/^\* / { print $2 }') --git-author --release ${VERSION} ${DISTRIBUTION} ${URGENCY} ${ALLOW_LOWER_VERSION} ${NO_EDITOR} --since ${COMMIT} ${_CUSTOMIZATION}
	gbp dch --debian-branch $(git branch | awk '/^\* / { print $2 }') --git-author --release ${VERSION} ${DISTRIBUTION} ${URGENCY} ${ALLOW_LOWER_VERSION} ${NO_EDITOR} --since ${COMMIT} ${_CUSTOMIZATION}
else
	#git-dch --auto --debian-branch $(git branch | awk '/^\* / { print $2 }') --git-author --release ${_CUSTOMIZATION} ${@}
	gbp dch --auto --debian-branch $(git branch | awk '/^\* / { print $2 }') --git-author --release ${_CUSTOMIZATION} ${@}
fi
